# Heart Oscillations during Meditation

This project is part of the class Advanced Signal Processing, teached in ECE Auth, Semester 8. 

For this project, the team will study the heart rate Oscillations during Meditation. The samples were taken from [here](https://physionet.org/content/meditation/1.0.0/), while the goal of the project is described in the uploaded pdf file (in Greek, some details will be shared in here as well later on).

The basic language of the project is MATLAB

All the data are handled from the Main.m file

The LoadData.m  is a script that helps load all the data in the main script
The PrintGraphs.m is a simple function that allows to plot the original signals from each category
The wavelets.m  allows us to plot the wavelets from each category
