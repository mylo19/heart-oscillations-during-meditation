%% YOGA

close all

signal = ChiMed1r(31:end-1,2);
level = 4; 
wavelet = 'db2';
wpt = wpdec(signal1, level, wavelet, 'shannon');

sizeLevel = size(wpcoef(wpt,[4 0]));

node30 = idwt(wpcoef(wpt,[4 0]), wpcoef(wpt,[4 1]), wavelet);
node31 = idwt(zeros(sizeLevel), wpcoef(wpt,[4 3]), wavelet);
node32 = zeros(size(node31));
node33 = idwt(wpcoef(wpt,[4 6]), wpcoef(wpt,[4 7]), wavelet);
node34 = idwt(zeros(sizeLevel), wpcoef(wpt,[4 9]), wavelet);
node35 = idwt(wpcoef(wpt,[4 10]), wpcoef(wpt,[4 11]), wavelet);
node36 = idwt(wpcoef(wpt,[4 12]), wpcoef(wpt,[4 13]), wavelet);
node37 = idwt(wpcoef(wpt,[4 14]), zeros(sizeLevel), wavelet);

node20 = idwt(node30, node31, wavelet);
node21 = idwt(node32, node33, wavelet);
node22 = idwt(node34, node35, wavelet);
node23 = idwt(node36, node37, wavelet);

node10 = idwt(node20, node21, wavelet);
node11 = idwt(node22, node23, wavelet);

reconstructedlSample = idwt(node10, node11, wavelet);

figure()
plot(signal)
title('Original')

figure()
plot(reconstructedlSample)
title('Reconstructed')

M = ceil(length(signal)/32);


figure
[Bspec_Direct, waxis3] = bispecdx(signal,signal,signal,1,M,1,M);
legend('Original')

figure
[Bspec_Direct1, ~] = bispecdx(reconstructedlSample,reconstructedlSample,reconstructedlSample,M,1,M);
legend('Reconstructed')