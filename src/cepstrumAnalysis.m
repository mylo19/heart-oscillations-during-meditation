signal = (YogaMed1(:,2));

wavelet = 'db6'; % wavelet used
level = 1;
[c,l] = wavedec(signal,level, wavelet);
approx = appcoef(c,l, wavelet);
cd = detcoef(c,l,level);

%plot(signal)
cepstrum = cceps(cd);

figure()
plot(cepstrum)

frame = 48;

for i=1:frame:length(cd)-frame
    windowedSignal(i:i + frame) = cd(i:i + frame).* hamming(frame + 1);
end

liftered_cepstrum = cceps(windowedSignal);
figure()
plot(liftered_cepstrum)