function wavelets(category)
%an eazy way to print the data that we want 
%Valid inputs:
%'chiPre','chiMed','yogaPre,'yogaMed','metron','normal','ironman'

loadData
switch category
    case 'chiPre'
   
        mesh (abs(cwt(ChiPre1(:,2))));
        title('wavelet ChiPre1')
        figure();
        mesh (abs(cwt(ChiPre2(:,2))));
        title('wavelet ChiPre2')
        figure();
        mesh (abs(cwt(ChiPre3(:,2))));
        title('wavelet ChiPre3')
        figure();
        mesh (abs(cwt(ChiPre4(:,2))));
        title('wavelet ChiPre4')
        figure();
        mesh (abs(cwt(ChiPre5(:,2))));
        title('wavelet ChiPre5')
        figure();
        mesh (abs(cwt(ChiPre6(:,2))));
        title('wavelet ChiPre6')
        figure();
        mesh (abs(cwt(ChiPre7(:,2))));
        title('wavelet ChiPre7')
        figure();
        mesh (abs(cwt(ChiPre8(:,2))));
        title('wavelet ChiPre8')
        
        
        
    case 'chiMed'
        mesh (abs(cwt(ChiMed1(:,2))));
        title('wavelet ChiMed1')
        figure();
        mesh (abs(cwt(ChiMed2(:,2))));
        title('wavelet ChiMed2')
        figure();
        mesh (abs(cwt(ChiMed3(:,2))));
        title('wavelet ChiMed3')
        figure();
        mesh (abs(cwt(ChiMed4(:,2))));
        title('wavelet ChiMed4')
        figure();
        mesh (abs(cwt(ChiMed5(:,2))));
        title('wavelet ChiMed5')
        figure();
        mesh (abs(cwt(ChiMed6(:,2))));
        title('wavelet ChiMed6')
        figure();
        mesh (abs(cwt(ChiMed7(:,2))));
        title('wavelet ChiMed7')
        figure();
        mesh (abs(cwt(ChiMed8(:,2))));
        title('wavelet ChiMed8')
        
    case 'yogaPre'
        mesh (abs(cwt(YogaPre1(:,2))));
        title('wavelet YogaPre1')
        figure();
        mesh (abs(cwt(YogaPre2(:,2))));
        title('wavelet YogaPre2')
        figure();
        mesh (abs(cwt(YogaPre3(:,2))));
        title('wavelet YogaPre3')
        figure();
        mesh (abs(cwt(YogaPre4(:,2))));
        title('wavelet YogaPre4')
     case 'yogaMed'
         mesh (abs(cwt(YogaMed1(:,2))));
        title('wavelet YogaMed1')
        figure();
        mesh (abs(cwt(YogaMed2(:,2))));
        title('wavelet YogaMed2')
        figure();
        mesh (abs(cwt(YogaMed3(:,2))));
        title('wavelet YogaMed3')
        figure();
        mesh (abs(cwt(YogaMed4(:,2))));
        title('wavelet YogaMed4')
    case 'ironman'
        mesh (abs(cwt(Ironman1(:,2))));
        title('wavelet Ironman1')
        figure();
        mesh (abs(cwt(Ironman2(:,2))));
        title('wavelet Ironman2')
        figure();
        mesh (abs(cwt(Ironman3(:,2))));
        title('wavelet Ironman3')
        figure();
        mesh (abs(cwt(Ironman4(:,2))));
        title('wavelet Ironman4')
        figure();
        mesh (abs(cwt(Ironman5(:,2))));
        title('wavelet Ironman5')
        figure();
        mesh (abs(cwt(Ironman6(:,2))));
        title('wavelet Ironman6')
        figure();
        mesh (abs(cwt(Ironman7(:,2))));
        title('wavelet Ironman7')
        figure();
        mesh (abs(cwt(Ironman8(:,2))));
        title('wavelet Ironman8') 
        mesh (abs(cwt(Ironman9(:,2))));
        title('wavelet Ironman9') 
    case 'metron'
        mesh (abs(cwt(Metron1(:,2))));
        title('wavelet Metron1')
        figure();
        mesh (abs(cwt(Metron2(:,2))));
        title('wavelet Metron2')
        figure();
        mesh (abs(cwt(Metron3(:,2))));
        title('wavelet Metron3')
        figure();
        mesh (abs(cwt(Metron4(:,2))));
        title('wavelet Metron4')
        figure();
        mesh (abs(cwt(Metron5(:,2))));
        title('wavelet Metron5')
        figure();
        mesh (abs(cwt(Metron6(:,2))));
        title('wavelet Metron6')
        figure();
        mesh (abs(cwt(Metron7(:,2))));
        title('wavelet Metron7')
        figure();
        mesh (abs(cwt(Metron8(:,2))));
        title('wavelet Metron8') 
        mesh (abs(cwt(Metron9(:,2))));
        title('wavelet Metron9')
        mesh (abs(cwt(Metron10(:,2))));
        title('wavelet Metron10')
        figure();
        mesh (abs(cwt(Metron11(:,2))));
        title('wavelet Metron11')
        figure();
        mesh (abs(cwt(Metron12(:,2))));
        title('wavelet Metron12')
        figure();
        mesh (abs(cwt(Metron13(:,2))));
        title('wavelet Metron13')
        figure();
        mesh (abs(cwt(Metron14(:,2))));
        title('wavelet Metron14')
    case 'normal'
         mesh (abs(cwt(Normal1(:,2))));
        title('wavelet Normal1')
        figure();
        mesh (abs(cwt(Normal2(:,2))));
        title('wavelet Normal2')
        figure();
        mesh (abs(cwt(Normal3(:,2))));
        title('wavelet Normal3')
        figure();
        mesh (abs(cwt(Normal4(:,2))));
        title('wavelet Normal4')
        figure();
        mesh (abs(cwt(Normal5(:,2))));
        title('wavelet Normal5')
        figure();
        mesh (abs(cwt(Normal6(:,2))));
        title('wavelet Normal6')
        figure();
        mesh (abs(cwt(Normal7(:,2))));
        title('wavelet Normal7')
        figure();
        mesh (abs(cwt(Normal8(:,2))));
        title('wavelet Normal8') 
        mesh (abs(cwt(Normal9(:,2))));
        title('wavelet Normal9')
        mesh (abs(cwt(Normal10(:,2))));
        title('wavelet Normal10')
        figure();
        mesh (abs(cwt(Normal11(:,2))));
        title('wavelet Normal11')
    otherwise
        disp('Invalid Input')
end
return