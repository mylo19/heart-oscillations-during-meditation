%close all
x = resampling(Normal1);

level = 5;
wavelet = 'db6';
wpt = wpdec(x, level, wavelet);

% examinedSignal = wpcoef(wpt, [4 1]);
% examinedSignal = examinedSignal/norm(examinedSignal);
% 
% x = examinedSignal;
x = x/norm(x);
M = ceil(length(x)/32);

figure
bispecdx(x,x,x,1,M,1,M);

% sizeLevel = size(wpcoef(wpt,[3 0]));
% 
% node20 = idwt(wpcoef(wpt, [3 0]), wpcoef(wpt, [3 1]), wavelet);
% node21 = idwt(zeros(sizeLevel), wpcoef(wpt, [3 3]), wavelet);
% node22 = zeros(size(node20));
% node23 = zeros(size(node20));
% 
% node10 = idwt(node20, node21, wavelet);
% node11 = idwt(node22, node23, wavelet);
% 
% y = idwt(node10, node11, wavelet);
% 
% figure
% plot(x)
% title('Original')
% 
% figure
% plot(y)
% title('Reconstructed')