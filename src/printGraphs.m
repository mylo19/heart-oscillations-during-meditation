function PrintGraphs(category)
%an eazy way to print the data that we want 
%Valid inputs:
%'chiPre','chiMed','yogaPre,'yogaMed','metron','normal','ironman'

LoadData
switch category
    case 'chiPre'
   
        plot (ChiPre1(:,2));
        title('ChiPre1')
        figure();
        plot (ChiPre2(:,2));
        title('ChiPre2')
        figure();
        plot (ChiPre3(:,2));
        title('ChiPre3')
        figure();
        plot (ChiPre4(:,2));
        title('ChiPre4')
        figure();
        plot (ChiPre5(:,2));
        title('ChiPre5')
        figure();
        plot (ChiPre6(:,2));
        title('ChiPre6')
        figure();
        plot (ChiPre7(:,2));
        title('ChiPre7')
        figure();
        plot (ChiPre8(:,2));
        title('ChiPre8')
        
        
        
    case 'chiMed'
        plot (ChiMed1(:,2));
        title('ChiMed1')
        figure();
        plot (ChiMed2(:,2));
        title('ChiMed2')
        figure();
        plot (ChiMed3(:,2));
        title('ChiMed3')
        figure();
        plot (ChiMed4(:,2));
        title('ChiMed4')
        figure();
        plot (ChiMed5(:,2));
        title('ChiMed5')
        figure();
        plot (ChiMed6(:,2));
        title('ChiMed6')
        figure();
        plot (ChiMed7(:,2));
        title('ChiMed7')
        figure();
        plot (ChiMed8(:,2));
        title('ChiMed8')
        
    case 'yogaPre'
        plot (YogaPre1(:,2));
        title('YogaPre1')
        figure();
        plot (YogaPre2(:,2));
        title('YogaPre2')
        figure();
        plot (YogaPre3(:,2));
        title('YogaPre3')
        figure();
        plot (YogaPre4(:,2));
        title('YogaPre4')
     case 'yogaMed'
         plot (YogaMed1(:,2));
        title('YogaMed1')
        figure();
        plot (YogaMed2(:,2));
        title('YogaMed2')
        figure();
        plot (YogaMed3(:,2));
        title('YogaMed3')
        figure();
        plot (YogaMed4(:,2));
        title('YogaMed4')
    case 'ironman'
        plot (Ironman1(:,2));
        title('Ironman1')
        figure();
        plot (Ironman2(:,2));
        title('Ironman2')
        figure();
        plot (Ironman3(:,2));
        title('Ironman3')
        figure();
        plot (Ironman4(:,2));
        title('Ironman4')
        figure();
        plot (Ironman5(:,2));
        title('Ironman5')
        figure();
        plot (Ironman6(:,2));
        title('Ironman6')
        figure();
        plot (Ironman7(:,2));
        title('Ironman7')
        figure();
        plot (Ironman8(:,2));
        title('Ironman8') 
        plot (Ironman9(:,2));
        title('Ironman9') 
    case 'metron'
        plot (Metron1(:,2));
        title('Metron1')
        figure();
        plot (Metron2(:,2));
        title('Metron2')
        figure();
        plot (Metron3(:,2));
        title('Metron3')
        figure();
        plot (Metron4(:,2));
        title('Metron4')
        figure();
        plot (Metron5(:,2));
        title('Metron5')
        figure();
        plot (Metron6(:,2));
        title('Metron6')
        figure();
        plot (Metron7(:,2));
        title('Metron7')
        figure();
        plot (Metron8(:,2));
        title('Metron8') 
        plot (Metron9(:,2));
        title('Metron9')
        plot (Metron10(:,2));
        title('Metron10')
        figure();
        plot (Metron11(:,2));
        title('Metron11')
        figure();
        plot (Metron12(:,2));
        title('Metron12')
        figure();
        plot (Metron13(:,2));
        title('Metron13')
        figure();
        plot (Metron14(:,2));
        title('Metron14')
    case 'normal'
         plot (Normal1(:,2));
        title('Normal1')
        figure();
        plot (Normal2(:,2));
        title('Normal2')
        figure();
        plot (Normal3(:,2));
        title('Normal3')
        figure();
        plot (Normal4(:,2));
        title('Normal4')
        figure();
        plot (Normal5(:,2));
        title('Normal5')
        figure();
        plot (Normal6(:,2));
        title('Normal6')
        figure();
        plot (Normal7(:,2));
        title('Normal7')
        figure();
        plot (Normal8(:,2));
        title('Normal8') 
        plot (Normal9(:,2));
        title('Normal9')
        plot (Normal10(:,2));
        title('Normal10')
        figure();
        plot (Normal11(:,2));
        title('Normal11')
    otherwise
        disp('Invalid Input')
end
return