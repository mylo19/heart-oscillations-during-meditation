function [x, fs] = resampling(signal)

fs = 2;%length(signal(:,2))/(signal(end,1) - signal(1,1));
x = resample(signal(:,2), signal(:,1), fs);

end
