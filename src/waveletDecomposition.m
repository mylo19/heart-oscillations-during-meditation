% loadData


signal = ChiPre1(:,2); % Signal to be examined

level = 2;
wavelet = 'db2'; % wavelet used

[c,l] = wavedec(signal,level, wavelet);
approx = appcoef(c,l, wavelet);
cd = detcoef(c,l,level);

figure()
plot(signal)
title('Original Signal')

figure()
plot(approx)
title(['Approximation of signal, level ', num2str(level)])

figure()
plot(cd)
title(['Detailed coefficients of signal, level ', num2str(level)])

% calculate cirtosis

% examinedSignal = approx;
% meanValue = mean(examinedSignal);
% N = length(examinedSignal);
% sigma = std(examinedSignal);
% 
% gamma3 = sum((examinedSignal - meanValue).^3)/((N - 1)*sigma^3);
% disp(gamma3)
% 
% gamma4 = kurtosis(signal);
% %disp (gamma4)
