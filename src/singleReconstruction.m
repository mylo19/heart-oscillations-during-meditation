%% Reconstruction
%close all

[signal, fs] = resampling(YogaMed1);
level = 5;
wavelet = 'db6';

wpt = wpdec(signal, level, wavelet);

sizeLevel = size(wpcoef(wpt,[5 0]));

node40 = idwt(zeros(sizeLevel), wpcoef(wpt,[5 1]), wavelet); %idwt(wpcoef(wpt,[5 0]), zeros(sizeLevel), wavelet);
node41 = idwt(wpcoef(wpt,[5 2]), wpcoef(wpt,[5 3]), wavelet);%zeros(size(node30));
node42 = zeros(size(node40));
node43 = zeros(size(node40));
node44 = zeros(size(node40));%idwt(wpcoef(wpt,[4 8]), zeros(sizeLevel), wavelet);
node45 = zeros(size(node40));%idwt(zeros(sizeLevel), wpcoef(wpt,[4 11]), wavelet);
node46 = zeros(size(node40));
node47 = zeros(size(node40));

node30 = idwt(node40, node41, wavelet);
node31 = idwt(node42, node43, wavelet);%zeros(size(node30));
node32 = zeros(size(node30));
node33 = zeros(size(node30));
node34 = zeros(size(node30));%idwt(wpcoef(wpt,[4 8]), zeros(sizeLevel), wavelet);
node35 = zeros(size(node30));%idwt(zeros(sizeLevel), wpcoef(wpt,[4 11]), wavelet);
node36 = zeros(size(node30));
node37 = zeros(size(node30));

node20 = idwt(node30, node31, wavelet);
node21 = idwt(node32, node33, wavelet);
node22 = idwt(node34, node35, wavelet);
node23 = idwt(node36, node37, wavelet);

node10 = idwt(node20, node21, wavelet);
node11 = idwt(node22, node23, wavelet);

recSignal = idwt(node10, node11, wavelet);

% figure()
% plot(signal)
% title('Original')
% 
% figure()
% plot(recSignal)
% title('Reconstructed')

% M = ceil(length(signal)/8);
% 
% figure
% [Bspec_Direct, waxis3] = bispecdx(signal,signal,signal,fs,M,1,M);
% legend('Original')
% 
% figure
% [Bspec_Direct1, waxis] = bispecdx(recSignal,recSignal,recSignal,fs, M,1,M);
% legend('Reconstructed')