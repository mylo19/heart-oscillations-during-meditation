signal = resampling(YogaMed1);
stationarySignal = diff(signal);

ps = periodogram(stationarySignal, [], length(stationarySignal), 1, 'centered');
step = 2/length(signal);
frequency = -1:step:1;

figure
plot(frequency(2:end-1),ps)

figure
mesh(real(cwt(signal)))


signal = resampling(Normal10);
stationarySignal = diff(signal);

ps = periodogram(stationarySignal, [], length(stationarySignal), 1, 'centered');

step = 1/length(signal);
frequency = -0.5:step:0.5;

figure
plot(ps)

figure
mesh(real(cwt(signal)))
