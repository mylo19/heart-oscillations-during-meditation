%% Load Data - Add the whole project to Path before running this code

%chi meditation

%Heartrate before chi meditation
ChiPre1=load('C1.pre');
ChiPre2=load('C2.pre');
ChiPre3=load('C3.pre');
ChiPre4=load('C4.pre');
ChiPre5=load('C5.pre');
ChiPre6=load('C6.pre');
ChiPre7=load('C7.pre');
ChiPre8=load('C8.pre');
    
%Heartrate during chi meditation
ChiMed1=load('C1.med');
ChiMed2=load('C2.med');
ChiMed3=load('C3.med');
ChiMed4=load('C4.med');
ChiMed5=load('C5.med');
ChiMed6=load('C6.med');
ChiMed7=load('C7.med');
ChiMed8=load('C8.med');


%Yoga meditation

%Heartrate before chi meditation
YogaPre1=load('Y1.pre');
YogaPre2=load('Y2.pre');
YogaPre3=load('Y3.pre');
YogaPre4=load('Y4.pre');   
    
%Heartrate during chi meditation
YogaMed1=load('Y1.med');
YogaMed2=load('Y2.med');
YogaMed3=load('Y3.med');
YogaMed4=load('Y4.med');


%Triathlon Athletes
    
Ironman1=load('I1');
Ironman2=load('I2');
Ironman3=load('I3');
Ironman4=load('I4');
Ironman5=load('I5');
Ironman6=load('I6');
Ironman7=load('I7');
Ironman8=load('I8');
Ironman9=load('I9');
    

%People doing metronomic breathing

Metron1=load('M1');
Metron2=load('M2');
Metron3=load('M3');
Metron4=load('M4');
Metron5=load('M5');
Metron6=load('M6');
Metron7=load('M7');
Metron8=load('M8');
Metron9=load('M9');
Metron10=load('M10');
Metron11=load('M11');
Metron12=load('M12');
Metron13=load('M13');
Metron14=load('M14');   
    
%People doing normal breathing

Normal1=load('N1');
Normal2=load('N2');
Normal3=load('N3');
Normal4=load('N4');
Normal5=load('N5');
Normal6=load('N6');
Normal7=load('N7');
Normal8=load('N8');
Normal9=load('N9');
Normal10=load('N10');
Normal11=load('N11');
