
level = 3; % level of decomposition

gamma3_med = zeros(level+1, 2^level, 8);
gamma4_med = zeros(level+1, 2^level, 8);

% level of decomposition
wavelet = 'db6'; % wavelet used


gamma3_pre = zeros(level+1, 2^level, 8);
gamma4_pre = zeros(level+1, 2^level, 8);

for k = 1:8
    switch k 
        case 1
            signal1 = resampling(ChiMed1);
            signal2 = resampling(ChiPre1);
        case 2
            signal1 = resampling(ChiMed1);
            signal2 = ChiPre2(:,2);
        case 3
            signal1 = ChiMed3(:,2);
            signal2 = ChiPre3(:,2);
        case 4
            signal1 = ChiMed4(:,2);
            signal2 = ChiPre4(:,2);
        case 5
            signal1 = ChiMed5(:,2);
            signal2 = ChiPre5(:,2);
        case 6
            signal1 = ChiMed6(:,2);
            signal2 = ChiPre6(:,2);
        case 7
            signal1 = ChiMed7(:,2);
            signal2 = ChiPre7(:,2);
        case 8
            signal1 = ChiMed8(:,2);
            signal2 = ChiPre8(:,2);
    end
    for i = 1:length(signal1)
        if isnan(signal1(i))
            signal1(i) = 0;
        end
    end
    for i = 1:length(signal2)
        if isnan(signal2(i))
            signal2(i) = 0;
        end
    end
   % signal = YogaPre2(:,2); % Signal to be examined
    wavelet = 'db2'; % wavelet used

    wpt1 = wpdec(signal1, level, wavelet, 'shannon');
    wpt2 = wpdec(signal2, level, wavelet, 'shannon');
%     figure
%     plot(wpt)

    % bestTree = besttree(wpt);
    % figure
    % plot(bestTree)


    for i = 0:level
        for j = 0: 2^i - 1

            examinedSignal1 = wpcoef(wpt1, [i j]);
            examinedSignal2 = wpcoef(wpt2, [i j]);
            
            meanValue = mean(examinedSignal1);
            N = length(examinedSignal1);
            sigma = std(examinedSignal1);

            gamma3_med(i+1, j+1,k) = sum((examinedSignal1 - meanValue).^3)/((N - 1)*sigma^3);
            
            gamma3_pre(i+1, j+1,k) = skewness(examinedSignal2);
            
            gamma4_pre(i+1, j+1,k) = kurtosis(examinedSignal2);

            gamma4_med(i+1, j+1,k) = kurtosis(examinedSignal1);

        end
    end
end

gamma3 = gamma3_pre - gamma3_med;
gamma4 = gamma4_pre - gamma4_med;
a = zeros(1,8);

close all

for i = 1:16
     for j = 1:8
         a(j) = gamma3(5,i,j);
     end
     figure
     plot(a)
end
