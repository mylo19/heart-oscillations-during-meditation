%close all

[signal, fs] = resampling(ChiMed4);
[wt, f] = cwt(signal,fs);

figure
mesh(real(wt))
xlabel('Time')
ylabel('Scale')
