close all

level = 8;
gamma3 = zeros(level+1, 2^level);
gamma4 = zeros(level+1, 2^level);
power = zeros(level+1, 2^level);

[signal] = diff(resampling(Metron5));
fs = 2;
initialPower = rms(signal)^2;
wavelet = 'db6';
wpt = wpdec(signal, level, wavelet);

for i = 0:level
    for j = 0:2^i-1
        examinedSignal = wpcoef(wpt, [i j]);
        gamma3(i+1, j+1) = skewness(examinedSignal);
        gamma4(i+1,j+1) = kurtosis(examinedSignal) - 3;  
        power(i+1, j+1) = rms(examinedSignal)^2;
    end
end
percentage = sum(power(5,1:2))/sum(power(5,:));
[wavelet, f] = cwt(signal, fs);
figure()
mesh(real(wavelet))
disp(power)
%disp(percentage)
%disp(gamma4(5,1))