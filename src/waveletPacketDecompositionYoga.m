level = 2;

gamma3_med = zeros(level+1, 2^level, 4);
gamma4_med = zeros(level+1, 2^level, 4);

gamma3_pre = zeros(level+1, 2^level, 4);
gamma4_pre = zeros(level+1, 2^level, 4);

for k = 1:4
    switch k 
        case 1
            signal1 = resampling(ChiMed1);
            signal2 = resampling(Normal2);
        case 2
            signal1 = resampling(ChiMed2);
            signal2 = resampling(Normal6);
        case 3
            signal1 = resampling(ChiMed3);
            signal2 = resampling(Normal8);
        case 4
            signal1 = resampling(ChiMed4);
            signal2 = resampling(Normal5);
    end
   % signal = YogaPre2(:,2); % Signal to be examined
     % level of decomposition
    wavelet = 'db6'; % wavelet used

    wpt1 = wpdec(signal1, level, wavelet, 'shannon');
    wpt2 = wpdec(signal2, level, wavelet, 'shannon');
%     figure
%     plot(wpt)

    % bestTree = besttree(wpt);
    % figure
    % plot(bestTree)


    for i = 0:level
        for j = 0: 2^i - 1

            examinedSignal1 = wpcoef(wpt1, [i j]);
            examinedSignal2 = wpcoef(wpt2, [i j]);
            
            meanValue = mean(examinedSignal1);
            N = length(examinedSignal1);
            sigma = std(examinedSignal1);

            gamma3_med(i+1, j+1,k) = sum((examinedSignal1 - meanValue).^3)/((N - 1)*sigma^3);
            
            gamma3_pre(i+1, j+1,k) = skewness(examinedSignal2);
            
            gamma4_pre(i+1, j+1,k) = kurtosis(examinedSignal2);

            gamma4_med(i+1, j+1,k) = kurtosis(examinedSignal1);

        end
    end
end

gamma3 = gamma3_pre - gamma3_med;
gamma4 = gamma4_pre - gamma4_med;
a = zeros(1,4);
b = zeros(1,4);

close all

% for i = 1:16
%      for j = 1:4
%          a(j) = gamma3(5,i,j);
%          b(j) = gamma4(5,i,j);
%      end
%      figure
%      plot(a)
%      title(['skewness ', num2str(i)])
%      figure
%      plot(b)
%      title(['Kurtosis ',num2str(i)])
% end
