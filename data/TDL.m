cwtcauchy = cwtft2(A,'wavelet','cauchy','scales',1,...
    'angles',0:pi/8:2*pi-pi/8);
cwtmexh = cwtft2(A,'wavelet','mexh','scales',1,...
    'angles',0:pi/8:2*pi-pi/8);

angz = {'0', 'pi/8', 'pi/4', '3pi/8', 'pi/2', '5pi/8', '3pi/4', ...
    '7pi/8','pi', '9pi/8', '5pi/4', '11pi/8', '3pi/2', ...
    '13pi/8' '7pi/4', '15pi/8'};

figure()

for angn = 1:length(angz)
    subplot(211)
    imagesc(abs(cwtmexh.cfs(:,:,1,1,angn)));
    title(['Mexican hat at ' angz(angn) 'radians']);
    subplot(212)
    imagesc(abs(cwtcauchy.cfs(:,:,1,1,angn)));
    title(['Cauchy wavelet at ' angz(angn) 'radians']);
    pause(1);
end